#!/usr/bin/python3
from flask import Flask, flash, request, redirect, url_for

from nif.annotation import *

from flask_cors import CORS
import re
import os
import shutil
from werkzeug.utils import secure_filename
import zipfile
import sys
import service

"""

then to start run:
export FLASK_APP=flaskController.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run

example calls:
curl -X GET localhost:5000/welcome

"""


app = Flask(__name__)
app.secret_key = "super secret key"
app.config['config'] = 'config.ini'
app.config['input_file'] = 'dummy.txt'
#sys.argv.extend(['-config', 'config.ini'])
#sys.argv.extend(['--input_file', 'dummy.txt'])
kwargs = {x.split(':')[0].strip():x.split(':')[1].strip() for x in open('config.ini').readlines() if re.search(':',\
 x)}


app.config['save_config'] = 'save.config'
app.config['model'] = 'models/cnndmbert_transformer2_step_90000.pt'
app.config['fp32'] = False
app.config['avg_raw_probs'] = False
app.config['data_type'] = kwargs['data_type']
app.config['src'] = kwargs['src']
app.config['src_dir'] = kwargs['src_dir']
app.config['tgt'] = 'tgt.out'
app.config['shard_size'] = int(kwargs['shard_size'])
app.config['output'] = kwargs['output']
app.config['report_bleu'] = False
app.config['report_rouge'] = False
app.config['report_time'] = False
app.config['dynamic_dict'] = False
app.config['share_vocab'] = False
app.config['random_sampling_topk'] = int(kwargs['random_sampling_topk'])
app.config['random_sampling_temp'] = float(kwargs['random_sampling_temp'])
app.config['seed'] = int(kwargs['seed'])
app.config['beam_size'] = int(kwargs['beam_size'])
app.config['min_length'] = int(kwargs['min_length'])
app.config['max_length'] = int(kwargs['max_length'])
app.config['stepwise_penalty'] = kwargs['stepwise_penalty']
app.config['length_penalty'] = kwargs['length_penalty']
app.config['ratio'] = float(kwargs['ratio'])
app.config['coverage_penalty'] = kwargs['coverage_penalty']
app.config['alpha'] = float(kwargs['alpha'])
app.config['beta'] = float(kwargs['beta'])
app.config['block_ngram_repeat'] = int(kwargs['block_ngram_repeat'])
app.config['ignore_when_blocking'] = False
app.config['replace_unk'] = False
app.config['phrase_table'] = False
app.config['verbose'] = kwargs['verbose']
app.config['log_file'] = kwargs['log_file']
app.config['log_file_level'] = kwargs['log_file_level']
app.config['attn_debug'] = False
app.config['dump_beam'] = False
app.config['n_best'] = int(kwargs['n_best'])
app.config['batch_size'] = int(kwargs['batch_size'])
app.config['gpu'] = kwargs['gpu']
app.config['sample_rate'] = int(kwargs['sample_rate'])
app.config['window_size'] = float(kwargs['window_size'])
app.config['window_stride'] = float(kwargs['window_stride'])
app.config['window'] = kwargs['window']
app.config['image_channel_size'] = int(kwargs['image_channel_size'])
for k, v in app.config.items():
    sys.argv.extend(['--%s' % k, v])


CORS(app)

@app.route('/srv-summarize/analyzeText', methods=['POST'])
def summarize():
    if request.method == 'POST':
        cType = request.headers["Content-Type"]
        accept = request.headers["Accept"]
        data=request.stream.read().decode("utf-8")
        if accept == 'text/turtle':
            pass
        else:
            return 'ERROR: the Accept header '+accept+' is not supported!'
        if cType == 'text/plain':
            uri_prefix="http://lynx-project.eu/res/"+str(uuid.uuid4())
            d = NIFDocument.from_text(data, uri_prefix)
        elif cType == 'text/turtle':
            d = NIFDocument.parse_rdf(data, format='turtle')
        else:
            return 'ERROR: the contentType header '+cType+' is not supported!'

        #annotatedNIF = service.analyzeNIF(d)
        summ = service.analyzeNIF(d)
        #return annotatedNIF.serialize(format="ttl")
        return summ.serialize(format="ttl")
    else:
        return 'ERROR, only POST method allowed.'

if __name__ == '__main__':
    port = int(os.environ.get('PORT',5000))
    app.run(host='localhost', port=port, debug=True)
