#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from itertools import repeat
from onmt.utils.logging import init_logger
from onmt.utils.misc import split_corpus
from onmt.translate.translator import build_translator
import onmt.opts as opts
from onmt.utils.parse import ArgumentParser
from pytorch_transformers import *
import unicodedata
import csv
import nltk
import os
import sys
import re
import math
import operator
import numpy as np
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import sent_tokenize,word_tokenize
from nltk.corpus import stopwords
from operator import itemgetter 
import statistics as st
import itertools
import torch
import fileinput

wordlemmatizer = WordNetLemmatizer()
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
Stopwords = set(stopwords.words('english'))
wordlemmatizer = WordNetLemmatizer()
model = BertForNextSentencePrediction.from_pretrained('bert-base-uncased')
model.eval()

def strip_accents(s):
   return ''.join(c for c in unicodedata.normalize('NFKC', s))

def _get_parser():
    parser = ArgumentParser(description='translate.py')

    opts.config_opts(parser)
    opts.translate_opts(parser)
    return parser

def lemmatize_words(words):
    lemmatized_words = []
    for word in words:
       lemmatized_words.append(wordlemmatizer.lemmatize(word))
    return lemmatized_words

def stem_words(words):
    stemmed_words = []
    for word in words:
       stemmed_words.append(stemmer.stem(word))
    return stemmed_words

def remove_special_characters(text):
    regex = r'[^a-zA-Z0-9\s]'
    text = re.sub(regex,'',text)
    return text

def freq(words):
    words = [word.lower() for word in words]
    dict_freq = {}
    words_unique = []
    for word in words:
       if word not in words_unique:
           words_unique.append(word)
    for word in words_unique:
       dict_freq[word] = words.count(word)
    return dict_freq

def pos_tagging(text):
    pos_tag = nltk.pos_tag(text.split())
    pos_tagged_noun_verb = []
    for word,tag in pos_tag:
        if tag == "NN" or tag == "NNP" or tag == "NNS" or tag == "VB" or tag == "VBD" or tag == "VBG" or tag == "VBN" or tag == "VBP" or tag == "VBZ":
             pos_tagged_noun_verb.append(word)
    return pos_tagged_noun_verb

def tf_score(word,sentence):
    freq_sum = 0
    word_frequency_in_sentence = 0
    len_sentence = len(sentence)
    for word_in_sentence in sentence.split():
        if word == word_in_sentence:
            word_frequency_in_sentence = word_frequency_in_sentence + 1
    tf =  word_frequency_in_sentence/ len_sentence
    return tf

def idf_score(no_of_sentences,word,sentences):
    no_of_sentence_containing_word = 0
    for sentence in sentences:
        sentence = remove_special_characters(str(sentence))
        sentence = re.sub(r'\d+', '', sentence)
        sentence = sentence.split()
        sentence = [word for word in sentence if word.lower() not in Stopwords and len(word)>1]
        sentence = [word.lower() for word in sentence]
        sentence = [wordlemmatizer.lemmatize(word) for word in sentence]
        if word in sentence:
            no_of_sentence_containing_word = no_of_sentence_containing_word + 1
    idf = math.log10(no_of_sentences/no_of_sentence_containing_word)
    return idf

def tf_idf_score(tf,idf):
    return tf*idf

def word_tfidf(dict_freq,word,sentences,sentence):
    word_tfidf = []
    tf = tf_score(word,sentence)
    idf = idf_score(len(sentences),word,sentences)
    tf_idf = tf_idf_score(tf,idf)
    return tf_idf

def sentence_importance(sentence,dict_freq,sentences):
     sentence_score = 0
     sentence = remove_special_characters(str(sentence)) 
     sentence = re.sub(r'\d+', '', sentence)
     pos_tagged_sentence = [] 
     no_of_sentences = len(sentences)
     pos_tagged_sentence = pos_tagging(sentence)
     for word in pos_tagged_sentence:
          if word.lower() not in Stopwords and word not in Stopwords and len(word)>1: 
                word = word.lower()
                word = wordlemmatizer.lemmatize(word)
                sentence_score = sentence_score + word_tfidf(dict_freq,word,sentences,sentence)
     return sentence_score

def bound_substraction(xmin,x):
    return max(xmin,x-1)

def bound_summ(xmax,x):
    return min(xmax,x+1)

def calculate_bert(first_no,second_no,tokenized_sentence):

    first_sent = " ".join(tokenizer.tokenize(tokenized_sentence[first_no]))
    second_sent = " ".join(tokenizer.tokenize(tokenized_sentence[second_no]))
    length0 = len(first_sent.split())+2
    length1 = len(second_sent.split())+1
        
    sent = '[CLS] ' + first_sent + ' [SEP] ' +second_sent + ' [SEP]'
    segments_ids = [0] * length0 +[1] * length1
        
    tokenized_sent  = sent.split()
    if len(tokenized_sent)>=512: return torch.tensor([[1,-1]])
    indexed_tokens = tokenizer.convert_tokens_to_ids(tokenized_sent)
    tokens_tensor = torch.tensor([indexed_tokens])
    segments_tensors = torch.tensor([segments_ids])
    tokens_tensor = tokens_tensor
    segments_tensors = segments_tensors
        
    with torch.no_grad():
        predictions = model(tokens_tensor, segments_tensors)
    return predictions

def build_summary(sentence_with_importance,tokenized_sentence, no_of_sentences):
    cnt = 3
    summary = []
    sentence_no = []
    sentence_with_importance_sorted = sorted(sentence_with_importance.items(), key=operator.itemgetter(1),reverse=True)
    
    if 0 not in sentence_no: sentence_no.append(0)
    if 1 not in sentence_no: sentence_no.append(1)
    if 2 not in sentence_no: sentence_no.append(2)
        
    for word_prob in sentence_with_importance_sorted:
        if cnt < no_of_sentences and word_prob[0] not in sentence_no:
            sentence_no.append(word_prob[0])
            cnt = cnt+1

    sentence_no.sort()

    sentence_no_bert =[]

    sentence_no_bert = list(itertools.chain.from_iterable(itertools.repeat(x, 2) for x in sentence_no))
    sentence_no_bert.pop(0)
    sentence_no_bert.pop(len(sentence_no_bert)-1)
    sentence_no += build_consistency(sentence_no_bert,sentence_with_importance,tokenized_sentence, []) 
    sentence_no.sort()

    cnt = 0
    for sentence in tokenized_sentence:
        if cnt in sentence_no:
            summary.append(sentence)
        cnt = cnt+1

    return summary

def build_consistency(sentence_no,sentence_with_importance,tokenized_sentence, tested_sentenses):
    selected_sentences = []
    for i in range(0,len(sentence_no)-1,2):
        first_no = sentence_no[i]
        second_no = sentence_no[i+1]
        if (first_no+1) != second_no: predictions = calculate_bert(first_no,second_no,tokenized_sentence)
        else: continue
        if predictions[0].data[0].data.tolist()[0] < 0 and (first_no+1) < second_no:
            #print('New stop')

            hypotheses_indexes = range(first_no+1, second_no)
            best_sent = None
            best_sent_score = 0
            sentence_no_new = []
            for hyp in hypotheses_indexes:
                sentence_no_new += [hyp]
            for middle_no in sentence_no_new:
                hyp_score0 = calculate_bert(first_no,middle_no,tokenized_sentence)
                hyp_score1 = calculate_bert(middle_no,second_no,tokenized_sentence)
                #print("Number of the sentence: {:.2f}: left probability = {:.2f}, right prabability = {:.2f}, tf_idf = {:.2f}".format(middle_no,hyp_score0.data[0].data.tolist()[0],hyp_score1.data[0].data.tolist()[0],sentence_with_importance[middle_no]))
                if hyp_score0[0].data[0].data.tolist()[0]>0 and hyp_score1[0].data[0].data.tolist()[0] > 0:
                    new_score = (10+hyp_score0[0].data[0].data.tolist()[0])*sentence_with_importance[middle_no]+(10+hyp_score1[0].data[0].data.tolist()[0])*sentence_with_importance[middle_no]
                    best_sent = [best_sent,middle_no][np.argmax([best_sent_score, new_score])]
                    best_sent_score = np.argmax([best_sent_score,new_score])
            if best_sent != None: selected_sentences.append(best_sent)
                
    #print(selected_sentences)
    return selected_sentences
def APIMain(inputtext):

    parser = _get_parser()
    if not os.path.exists(os.path.join(os.getcwd(), "config.ini")):
        sys.stderr.write("ERROR: Config.ini not found.")

    sys.argv.extend(['-config', 'config.ini']) # ugly hack. config.ini has to be there
    sys.argv.extend(['-input_file', 'dummy.txt']) # ugly hack. dummy.txt is never used
    opt = parser.parse_args()
    
    #Open text and tokenize text
    texts = [x for x in inputtext.split("\n") if x.strip()]

    summaries = []
    """	
    with open(opt.input_file, 'rt', encoding='utf-8') as f:
        i = 0
        for line in f:
            texts.append(line)
    """
    two_step = True
    #Summarize extractive
    for text_num,line in enumerate(texts):
        summaries.append('[CLS] '+ " ".join(tokenizer.tokenize(strip_accents(line))))
        if not two_step: 
            continue
        input_user = 100*512/len(tokenizer.tokenize(strip_accents(line)))
        tokenized_sentence = [] 
        separators = [0]
        separators += [m.end() for m in re.finditer("\?", line)]
        separators += [m.end() for m in re.finditer("\!", line)]
        separators += [m.end() for m in re.finditer("\. ' '", line)]
        separators += [x for x in [m.end() for m in re.finditer("\.", line)] if x+4 not in separators]
        separators.sort()
        pre_sent = ''
        for i in range(len(separators)-1):
            sent = line[separators[i]:separators[i+1]]
            if len(sent.split()) > 2: 
                tokenized_sentence.append(pre_sent + sent)
                pre_sent = ''
            else: pre_sent = pre_sent + sent 


        text = remove_special_characters(str(line))
        text = re.sub(r'\d+', '', text)
        tokenized_words_with_stopwords = word_tokenize(text)
        tokenized_words = [word for word in tokenized_words_with_stopwords if word not in Stopwords]
        tokenized_words = [word for word in tokenized_words if len(word) > 1]
        tokenized_words = [word.lower() for word in tokenized_words]
        tokenized_words = lemmatize_words(tokenized_words)

        word_freq = freq(tokenized_words)
        no_of_sentences = int((input_user * len(tokenized_sentence))/100)

        c = 0
        sentence_with_importance = {}
        for sent in tokenized_sentence:
            sentenceimp = sentence_importance(sent,word_freq,tokenized_sentence)
            sentence_with_importance[c] = sentenceimp
            c = c+1

        #sentence_with_importance = sorted(sentence_with_importance.items(), key=operator.itemgetter(1),reverse=True)

        summary = build_summary(sentence_with_importance,tokenized_sentence, no_of_sentences)

        summary = " ".join(summary)
        summary= " ".join(tokenizer.tokenize(strip_accents(summary)))
        summary = '[CLS] '+ summary
        summaries[text_num] = summary

    with open ('ext_summ_file.txt', "w", encoding='utf-8') as temp_f:
        for summ in summaries:
            temp_f.write(summ)
            temp_f.write("\n")

    #Summarize Abstractively
    ArgumentParser.validate_translate_opts(opt)
    logger = init_logger(opt.log_file)
    translator = build_translator(opt, logger=logger, report_score=True)
    src_shards = split_corpus(opt.src, opt.shard_size)
    tgt_shards = split_corpus(opt.tgt, opt.shard_size) \
        if opt.tgt is not None else repeat(None)
    shard_pairs = zip(src_shards, tgt_shards)

    for i, (src_shard, tgt_shard) in enumerate(shard_pairs):
        logger.info("Translating shard %d." % i)
        translator.translate(
            src=src_shard,
            tgt=tgt_shard,
            src_dir=opt.src_dir,
            batch_size=opt.batch_size,
            attn_debug=opt.attn_debug
            )

    for line in fileinput.FileInput("output.out",inplace=1):
         line=line.replace(" ##","")
         print(line)

    outputs = []
    with open ('output.out', "r+", encoding='utf-8') as out_f:
        for out in out_f:
        	outputs.append(out)

    return "\n".join(outputs)


def main(opt):

    #Open text and tokenize text
    texts = []
    summaries = []
    with open(opt.input_file, 'rt', encoding='utf-8') as f:
        i = 0
        for line in f:
            texts.append(line)
    two_step = True
    #Summarize extractive
    for text_num,line in enumerate(texts):
        summaries.append('[CLS] '+ " ".join(tokenizer.tokenize(strip_accents(line))))
        if not two_step: 
            continue
        input_user = 100*512/len(tokenizer.tokenize(strip_accents(line)))
        tokenized_sentence = [] 
        separators = [0]
        separators += [m.end() for m in re.finditer("\?", line)]
        separators += [m.end() for m in re.finditer("\!", line)]
        separators += [m.end() for m in re.finditer("\. ' '", line)]
        separators += [x for x in [m.end() for m in re.finditer("\.", line)] if x+4 not in separators]
        separators.sort()
        pre_sent = ''
        for i in range(len(separators)-1):
            sent = line[separators[i]:separators[i+1]]
            if len(sent.split()) > 2: 
                tokenized_sentence.append(pre_sent + sent)
                pre_sent = ''
            else: pre_sent = pre_sent + sent 


        text = remove_special_characters(str(line))
        text = re.sub(r'\d+', '', text)
        tokenized_words_with_stopwords = word_tokenize(text)
        tokenized_words = [word for word in tokenized_words_with_stopwords if word not in Stopwords]
        tokenized_words = [word for word in tokenized_words if len(word) > 1]
        tokenized_words = [word.lower() for word in tokenized_words]
        tokenized_words = lemmatize_words(tokenized_words)

        word_freq = freq(tokenized_words)
        no_of_sentences = int((input_user * len(tokenized_sentence))/100)

        c = 0
        sentence_with_importance = {}
        for sent in tokenized_sentence:
            sentenceimp = sentence_importance(sent,word_freq,tokenized_sentence)
            sentence_with_importance[c] = sentenceimp
            c = c+1

        #sentence_with_importance = sorted(sentence_with_importance.items(), key=operator.itemgetter(1),reverse=True)

        summary = build_summary(sentence_with_importance,tokenized_sentence, no_of_sentences)

        summary = " ".join(summary)
        summary= " ".join(tokenizer.tokenize(strip_accents(summary)))
        summary = '[CLS] '+ summary
        summaries[text_num] = summary

    with open ('ext_summ_file.txt', "w", encoding='utf-8') as temp_f:
        for summ in summaries:
            temp_f.write(summ)
            temp_f.write("\n")

    #Summarize Abstractively
    ArgumentParser.validate_translate_opts(opt)
    logger = init_logger(opt.log_file)
    translator = build_translator(opt, logger=logger, report_score=True)
    src_shards = split_corpus(opt.src, opt.shard_size)
    tgt_shards = split_corpus(opt.tgt, opt.shard_size) \
        if opt.tgt is not None else repeat(None)
    shard_pairs = zip(src_shards, tgt_shards)

    for i, (src_shard, tgt_shard) in enumerate(shard_pairs):
        logger.info("Translating shard %d." % i)
        translator.translate(
            src=src_shard,
            tgt=tgt_shard,
            src_dir=opt.src_dir,
            batch_size=opt.batch_size,
            attn_debug=opt.attn_debug
            )

    for line in fileinput.FileInput("output.out",inplace=1):
         line=line.replace(" ##","")
         print(line)
    #outputs = []
    #with open ('output.out', "r+", encoding='utf-8') as out_f:
    #    ind = 0
    #    for out in out_f:
    #        out_f.write(summ)
    #        temp_f.write("\n")
    #        ind += 1
    #outputs.appens(out.replace(' ##', '')
        

if __name__ == "__main__":
    parser = _get_parser()

    opt = parser.parse_args()
    import dill as pickle
    import codecs
    pickle.dump(opt, codecs.open('opts.pickle', 'wb'))
    #models/cnndmbert_transformer2_step_90000.pt
    opt2 = pickle.load(codecs.open('opts.pickle', 'rb'))
    print(opt2)
    #ext_summ_file.txt
    sys.exit()
    main(opt)

