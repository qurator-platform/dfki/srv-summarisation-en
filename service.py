from nltk.corpus import stopwords
import string
import os
from nif.annotation import *
import summarize



def analyzeNIF(nifDocument):
    d = nifDocument
    text = d.context.nif__is_string

    summary = analyzeText(text)
    #summary = "Summary of the text to be included in the NIF document."

    kwargs = {"nif__summary" : summary}
    nc = NIFContext(text,d.context.uri_prefix,**kwargs)
    d2 = NIFDocument(nc, structures=d.structures)
    return d2

def analyzeText(text):
    summary = summarize.APIMain(text)
    return summary

