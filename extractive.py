# -*- coding: utf-8 -*-
from pytorch_pretrained_bert import BertTokenizer
#from unidecode import unidecode
import unicodedata
import csv


def strip_accents(s):
   return ''.join(c for c in unicodedata.normalize('NFKC', s))
   #          if not unicodedata.name(c).endswith('ACCENT'))    

tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased', do_lower_case=False)
with open('/home/daksenov/datasets/germanWiki/data_train.csv', 'rt', encoding='utf-8') as f:
    with open ('/home/daksenov/datasets/germanWiki/src_bert.txt', "w", encoding='utf-8') as src_f:
        with open ('/home/daksenov/datasets/germanWiki/tgt_bert.txt', "w", encoding='utf-8') as tgt_f:
            for i,line in enumerate(csv.reader(f, quotechar='"', delimiter=',',quoting=csv.QUOTE_ALL, skipinitialspace=True)):
                line_replaced = line[0]
                line_replaced = line_replaced.replace('ü','ü').replace('ä','ä').replace('ö','ö')
                line_replaced = line_replaced.replace('Ö','Ö').replace('Ä','Ä').replace('Ü','Ü')
                line_replaced = line_replaced.replace('–','-').replace("‘","'").replace('’',"'").replace('‐','-').replace('”','"')
                summ_replaced = line[1]

                summ_replaced = summ_replaced.replace('ü','ü').replace('ä','ä').replace('ö','ö')
                summ_replaced = summ_replaced.replace('Ö','Ö').replace('Ä','Ä').replace('Ü','Ü')
                summ_replaced = summ_replaced.replace('–','-').replace("‘","'").replace('’',"'").replace('‐','-').replace('”','"')
                text_t= " ".join(tokenizer.tokenize(strip_accents(line_replaced)))
                summ_t= " ".join(tokenizer.tokenize(strip_accents(summ_replaced)))
                if i!=0:
                    src_f.write(text_t)
                    src_f.write("\n")

                    tgt_f.write(summ_t)
                    tgt_f.write("\n")
                if i%100==0:
                    print(i)

                #if '[UNK]' in text_t:
                #    print(line[0])
                #    print(text_t)
                #    print(i)

                #stf.write('"'+text_t.replace('"',"'") +'","'+summ_t.replace('"',"'")+'"')
                #stf.write("\n")