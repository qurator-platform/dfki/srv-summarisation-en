FROM pytorch/pytorch:latest
#FROM pytorch/pytorch:1.4-cuda10.1-cudnn7-runtime

RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y python3-dev emacs &&\
    apt-get update -y

#RUN git clone https://github.com/OpenNMT/OpenNMT-py.git && cd OpenNMT-py && pip install -r requirements.txt && python setup.py install
#RUN git clone https://github.com/nvidia/apex && cd apex && python setup.py install

ADD requirements.txt .
RUN pip install -r requirements.txt

RUN python -m nltk.downloader stopwords
RUN python -m nltk.downloader punkt
RUN python -m nltk.downloader wordnet
RUN python -m nltk.downloader averaged_perceptron_tagger

ADD config.ini .
ADD flaskController.py .
ADD service.py .
ADD summarize.py .
ADD translate.py .
ADD models ./models
ADD nif ./nif
ADD onmt ./onmt
ADD opts.pickle .

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

EXPOSE 8080

ENTRYPOINT FLASK_APP=flaskController.py flask run -h 0.0.0.0 -p 8080
#CMD ["/bin/bash"]
