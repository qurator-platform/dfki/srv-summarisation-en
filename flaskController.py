#!/usr/bin/python3
from flask import Flask, Response, flash, request, redirect, url_for

#from nif.annotation import *

from flask_cors import CORS
import re
import os
import shutil
from werkzeug.utils import secure_filename
import zipfile
import sys
import dill as pickle
import codecs
import service
import json
import uuid
from multiprocessing import Process, Queue

import time

"""

then to start run:
export FLASK_APP=flaskController.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run

example calls:
curl -X GET localhost:5000/welcome

"""


app = Flask(__name__)
app.secret_key = "super secret key"

tasks = {}

CORS(app)

@app.route('/async/srv-summarize/analyzeText', methods=['POST'])
def summarizeAsync():
    global tasks
    if request.method == 'POST':
        #cType = request.headers["Content-Type"]
        #accept = request.headers["Accept"]
        data=request.stream.read().decode("utf-8")
        #if accept == 'text/turtle':
        #    pass
        #else:
        #    return 'ERROR: the Accept header '+accept+' is not supported!'
        #if cType == 'text/plain':
        #    uri_prefix="http://lynx-project.eu/res/"+str(uuid.uuid4())
        #    d = NIFDocument.from_text(data, uri_prefix)
        #elif cType == 'text/turtle':
        #    d = NIFDocument.parse_rdf(data, format='turtle')
        #else:
        #    return 'ERROR: the contentType header '+cType+' is not supported!'

        taskid = uuid.uuid4().hex

        tasks[taskid] = {}
        tasks[taskid]['text'] = data
        tasks[taskid]['result'] = Queue()

        heavy_process = Process(  # Create a daemonic process with heavy "my_func"
            target=my_func,
            args=(taskid,)
            #daemon=True
        )
        heavy_process.start()
        return Response(taskid,
            mimetype='application/json',
            status=202
        )
        #annotatedNIF = service.analyzeNIF(d)
        #summ = service.analyzeNIF(d)
        #return annotatedNIF.serialize(format="ttl")
        #return summ.serialize(format="ttl")
    else:
        return 'ERROR, only POST method allowed.'

# Define some heavy function
def my_func(taskid):
    global tasks
    #print(taskid)
    task = tasks[taskid]
    #time.sleep(10)
    print("Summarization started...")
    #print(task['text'])
    #task['result'] = 'This is the summary'
    summ = service.analyzeText(task['text'])
    q = task['result']
    q.put(summ)
    tasks[taskid] = task
    print('...Summarization finished.')
    #print(tasks[taskid]['result'])
    #print(tasks)


@app.route('/async/srv-summarize/result/<taskid>', methods=['GET'])
def resultAsync(taskid):
    global tasks
    print('RESULT OF: ',taskid)
    #print(tasks)
    #print(tasks[taskid]['result'].get())
    if tasks[taskid]['result'].empty():
        #if 'result' not in tasks[taskid]:
        return Response(
            mimetype='application/json',
            status=202
        )
    else:
        summ = tasks[taskid]['result'].get()
        print('SUMM: ',summ)
        tasks[taskid]['result'].put(summ)
        # TODO Delete the task from the dictionary of tasks
        return Response(summ,
            mimetype='text/plain',
            status=200
        )


@app.route('/srv-summarize/analyzeText', methods=['POST'])
def summarize():
    #"""
    if request.method == 'POST':
        cType = request.headers["Content-Type"]
        accept = request.headers["Accept"]
        data=request.stream.read().decode("utf-8")
        if accept == 'text/turtle':
            pass
        else:
            return 'ERROR: the Accept header '+accept+' is not supported!'
        if cType == 'text/plain':
            uri_prefix="http://lynx-project.eu/res/"+str(uuid.uuid4())
            d = NIFDocument.from_text(data, uri_prefix)
        elif cType == 'text/turtle':
            d = NIFDocument.parse_rdf(data, format='turtle')
        else:
            return 'ERROR: the contentType header '+cType+' is not supported!'

        #annotatedNIF = service.analyzeNIF(d)
        summ = service.analyzeNIF(d)
        #return annotatedNIF.serialize(format="ttl")
        return summ.serialize(format="ttl")
    else:
        return 'ERROR, only POST method allowed.'

    #"""
    """
    # @ Ela: if you comment out the stuff above and uncomment the lines below, it works using parameters instead of headers. Note that it just always spits out turtle, only works with plaintext (txt) as input, so it's dummy params still, but we can worry about that after the demo.
    supported_informats = ['txt']
    supported_outformats = ['turtle', 'txt']
    inp = None
    if request.args.get('input') == None:
        if request.data == None:
            return 'Please provide some text as input.\n'
        else:
            inp = request.data.decode('utf-8')
    else:
        inp = request.args.get('input')
    if request.args.get('outformat') == None:
        return 'Please specify output format (currently supported: %s)\n' % str(supported_outformats)
    elif request.args.get('outformat') not in supported_outformats:
        return 'Output format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('outformat'), (str(supported_outformats)))
    if request.args.get('informat') == None:
        return 'Please specify input format (currently supported: %s)\n' % str(supported_informats)
    elif request.args.get('informat') not in supported_informats:
        return 'Input format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('informat'), (str(supported_informats)))
   
    
    informat = request.args.get('informat')
    outformat = request.args.get('outformat')
    if outformat == 'turtle':
        uri_prefix="http://lynx-project.eu/res/"+str(uuid.uuid4())
        d = NIFDocument.from_text(inp, uri_prefix)
        
        summ = service.analyzeNIF(d)
        return summ.serialize(format="ttl")
    elif outformat == 'txt':
        summ = service.analyzeText(inp)
        return summ
    """


def create_error(code, message):
    failure_response_dict = {
        "failure": {
            "errors": [{
                "code": code,
                "text": message
            }]
        }
    }
    return Response(response=json.dumps(failure_response_dict), status=500, content_type='application/json')


@app.route('/', methods=['POST'])
def summarizeELG():
    if request.method == 'POST':
        ctype = request.headers["Content-Type"]
        accept = request.headers["Accept"]
        if not request.data:
            return create_error("elg.request.missing",
                                "Error: No request provided in message")

        request_body = request.data.decode('utf-8')
        request_dict = json.loads(request_body)
        mimeType = request_dict['mimeType']

        if mimeType == 'text/plain':
            content = request_dict['content']
        else:
            return create_error("elg.request.text.mimeType.unsupported",
                                "Error: MIME type {} not supported by this service".format(mimeType))
        print("DEBUG: ")
        print("Content: {}".format(content))
        print("Response type: {}".format(accept))

        if ctype == 'application/json':
            summ = service.analyzeText(content)
            summary = []
            for shard in summ:
                shard_dict = {
                    "role": "string",
                    "content": shard,
                    "features": {
                        "text_type": "summary"
                    }
                }
                summary.append(shard_dict)
            response_dict = {
                "response": {
                    "type": "texts",
                    "texts": summary
                }
            }
            return Response(response=json.dumps(response_dict), status=200, content_type='application/json')
        else:
            return create_error("elg.request.type.unsupported",
                                'Request type {} not supported by this service'.format(ctype))
    else:
        return create_error("elg.request.invalid",
                            'Invalid request message')


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 8080))
    app.run(host='localhost', port=port, debug=True)
